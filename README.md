# Daffery Client Modpack

This mod pack was designed for the Daffery Minecraft server but is free to use by anyone. It's primarily geared toward the original tech mods from Tekkit Classic but has a few extras to assist with main world preservation and quicker resource harvesting.